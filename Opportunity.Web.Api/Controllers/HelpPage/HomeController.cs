﻿using System.Web.Mvc;

namespace Opportunity.Web.Api.Controllers.HelpPage
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return Redirect("/swagger");
        }
    }
}
﻿using System.Threading.Tasks;
using System.Web.Http;
using Opportunity.Domains.Enums;
using Opportunity.Services.Interfaces;

namespace Opportunity.Web.Api.Controllers
{
    public class IdentityController : ApiController
    {
        private readonly IUsersService _usersService;

        public IdentityController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IHttpActionResult> Register()
        {
            return Ok();
        }

        /// <summary>
        /// Login in Opportunity
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IHttpActionResult> Login(string uid)
        {
            var user = await _usersService.LoginAsync(uid, string.Empty, SocialAccountType.Opportunity);
            return Ok(user);
        }
    }
}

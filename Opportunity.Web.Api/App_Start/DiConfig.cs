﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Opportunity.DiModules;

namespace Opportunity.Web.Api
{
    /// <summary>
    /// 
    /// </summary>
    public class DiConfig
    {
        public static void RegisterDi()
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterModule<DataRepositoriesModule>();

            builder.RegisterModule<IdentityServicesModule>();

            var container = builder.Build();

            var resolver = new AutofacWebApiDependencyResolver(container);

            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }
    }
}
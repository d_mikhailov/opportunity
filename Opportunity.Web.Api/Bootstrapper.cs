﻿using System.Web.Http;
using System.Web.Routing;
using Microsoft.Owin;
using Opportunity.Web.Api.App_Start;
using Owin;

[assembly: OwinStartup(typeof(Opportunity.Web.Api.Bootstrapper))]

namespace Opportunity.Web.Api
{
    public class Bootstrapper
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            DiConfig.RegisterDi();
            //SwaggerConfig.Register(GlobalConfiguration.Configuration);
        }
    }
}

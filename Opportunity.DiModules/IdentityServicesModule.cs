﻿using Autofac;
using Opportunity.Services;
using Opportunity.Services.Interfaces;

namespace Opportunity.DiModules
{
    public class IdentityServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UsersService>()
                .As<IUsersService>();
        }
    }
}

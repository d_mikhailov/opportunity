﻿using System.Data.Entity;
using Autofac;
using Opportunity.Repositories.EntityFramework;

namespace Opportunity.DiModules
{
    public class DataRepositoriesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Context>()
                .As<DbContext>()
                .WithParameter("nameOrConnectionString", "SqlOpportunity");
        }
    }
}

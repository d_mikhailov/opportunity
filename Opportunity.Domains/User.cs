﻿using System;
using System.Collections.Generic;
using Opportunity.Domains.Enums;

namespace Opportunity.Domains
{
    /// <summary>
    /// Main user identity entity
    /// </summary>
    public class User
    {
        public User()
        {
            SocialAccounts = new HashSet<SocialAccount>();
            CreationDate = DateTimeOffset.UtcNow;
            UserStatus = UserStatus.Active;
        }

        public long UserId { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public UserStatus UserStatus { get; set; }

        public DateTimeOffset CreationDate { get; set; }

        #region Many-to-One relationship with SocialAccounts

        public virtual ICollection<SocialAccount>  SocialAccounts { get; set; }

        #endregion
    }
}

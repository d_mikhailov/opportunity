﻿namespace Opportunity.Domains.Enums
{
    public enum SocialAccountType
    {
        Opportunity = 0,
        Facebook,
        Google
    }
}

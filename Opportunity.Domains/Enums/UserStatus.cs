﻿namespace Opportunity.Domains.Enums
{
    public enum UserStatus
    {
        Blocked = 0,
        Active
    }
}

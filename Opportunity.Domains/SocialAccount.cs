﻿using System;
using Opportunity.Domains.Enums;

namespace Opportunity.Domains
{
    /// <summary>
    /// User's account as social account
    /// </summary>
    public class SocialAccount
    {
        public long SocialAccountId { get; set; }

        public string SocialAccountUid { get; set; }

        public DateTimeOffset JoinedDate { get; set; }

        public SocialAccountType SocialAccountType { get; set; }
        
        #region One-to-Many relationship with User

        public long UserId { get; set; }

        public User User { get; set; }

        #endregion
    }
}

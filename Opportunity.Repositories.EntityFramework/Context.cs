﻿using System.Data.Entity;
using Opportunity.Domains;

namespace Opportunity.Repositories.EntityFramework
{
    public class Context : DbContext
    {
        public Context() : base("SqlOpportunity") { }

        public Context(string nameOrConnectionString) : base(nameOrConnectionString) { }

        #region DbSets

        public DbSet<User> Users { get; set; }

        public DbSet<SocialAccount> SocialAccounts { get; set; }

        #endregion
    }
}

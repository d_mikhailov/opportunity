﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Opportunity.Domains;
using Opportunity.Domains.Enums;
using Opportunity.Services.Interfaces;
using Opportunity.Services.Interfaces.DTOs.Identity;

namespace Opportunity.Services
{
    public class UsersService : IUsersService
    {
        private readonly DbContext _context;

        public UsersService(DbContext context)
        {
            _context = context;
        }

        #region Queries

        /// <summary>
        /// Get User entity from Database by user Uid
        /// </summary>
        /// <param name="uid">User's Social Account uid</param>
        /// <param name="securityCombination"></param>
        /// <param name="socialAccountType"></param>
        /// <returns></returns>
        public async Task<LoginResultDto> LoginAsync(string uid, string securityCombination, SocialAccountType socialAccountType)
        {
            // TODO:D By type
            var user = await _context.Set<SocialAccount>()
                .Where(s => s.SocialAccountUid.Equals(uid, StringComparison.OrdinalIgnoreCase))
                .Select(s => s.User)
                .SingleOrDefaultAsync();

            return new LoginResultDto();
        }

        #endregion
    }
}

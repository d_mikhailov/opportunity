﻿using System.Threading.Tasks;
using Opportunity.Domains;
using Opportunity.Domains.Enums;
using Opportunity.Services.Interfaces.DTOs.Identity;

namespace Opportunity.Services.Interfaces
{
    public interface IUsersService
    {
        Task<LoginResultDto> LoginAsync(string uid, string securityCombination, SocialAccountType socialAccountType);
    }
}

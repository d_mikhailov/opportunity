﻿using System;
using Opportunity.Domains;
using Opportunity.Domains.Enums;

namespace Opportunity.Services.Interfaces.DTOs.Identity
{
    public class SocialAccountDto
    {
        public long SocialAccountId { get; set; }

        public string SocialAccountUid { get; set; }

        public DateTimeOffset JoinedDate { get; set; }

        public SocialAccountType SocialAccountType { get; set; }

        public long UserId { get; set; }

        public User User { get; set; }
    }
}

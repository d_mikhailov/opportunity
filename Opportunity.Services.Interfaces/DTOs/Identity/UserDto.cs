﻿using System;
using System.Collections.Generic;
using Opportunity.Domains;

namespace Opportunity.Services.Interfaces.DTOs.Identity
{
    public class UserDto
    {
        public long UserId { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTimeOffset CreationDate { get; set; }

        public bool IsActive { get; set; }

        public List<SocialAccount> SocialAccounts { get; set; }
    }
}

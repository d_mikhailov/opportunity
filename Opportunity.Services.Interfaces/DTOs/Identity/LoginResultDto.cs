﻿namespace Opportunity.Services.Interfaces.DTOs.Identity
{
    public class LoginResultDto
    {
        public UserDto User { get; set; }

        public bool IsSuccess { get; set; }

        public string Description { get; set; }
    }
}
